/*
 * 
 * 
 * 
 */
package parking.business;

import java.io.Serializable;
import parking.GUI.PlaceButton;
import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;

/**
 *
 * @author remyk_000
 */
public abstract class Place implements Serializable
{
    //Le véhicule qui a réservé.
    protected Vehicule reserve;
    //Le véhicule qui est actuellement sur la place.
    protected Vehicule vehiculePresent;
    //Le bouton pour l'affichage GUI
    //private PlaceButton button;
    
//===========================================================================================================
/*___________________________CONSTRUCTOR___________________________________________________________________*/
//===========================================================================================================


    
    public Place(){}//Place()
    
    
//===========================================================================================================
/*___________________________GETTERS AND SETTERS___________________________________________________________________*/
//===========================================================================================================
    public abstract String getType();

    public Vehicule getReserve()
    {
       return this.reserve;
    }//getReserve()
    
     public void setReserve(Vehicule Reserve)
    {
        this.reserve = Reserve;
    }//setReserve()
    
    public Vehicule getVehiculePresent() 
    {
        return vehiculePresent;
    }//getVehiculePresent()

    public void setVehiculePresent(Vehicule vehicule) 
    {
        this.vehiculePresent = vehicule;
    }//setVehiculePresent()

    /*public PlaceButton getButton()
    { 
        return button; 
    } //getBoutton()

    public void setButton(PlaceButton button)
    { 
        this.button = button; 
    } // setBoutton()*/


//===========================================================================================================
/*___________________________METHODS___________________________________________________________________*/
//===========================================================================================================
   
    public abstract void park(Vehicule vehicule) throws PlaceOccupeException, ParkingPleinException;

    
}//class Place