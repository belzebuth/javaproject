/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;

/**
 *
 * @author aze
 */
public class PlaceTransporteur extends Place{
    static final String type = "transporteur";
   
    public PlaceTransporteur() {
        super();
    } //PlaceTransporteur()
    
    

    @Override
    public String getType() {
        return PlaceTransporteur.type;
    } //getType()

    @Override
    public void park(Vehicule vehicule) throws PlaceOccupeException, ParkingPleinException {
        //Si la place est occupée, on lance une exception.
        try {
            if (null != this.vehiculePresent) throw new PlaceOccupeException();
            else if (this.reserve != null && this.reserve != vehicule) throw new PlaceOccupeException();
            else {
                //Sinon, on gare le véhicule sur la place.
                vehicule.setPlaceOccupe(this);
                this.setVehiculePresent(vehicule);
            }
        }catch (PlaceOccupeException e) {
            System.out.println ("Mauvaise place");
        }
    } //park()
    
    
    @Override
    public String toString() 
    {
        if (vehiculePresent != null) return "Transporteur" +"\n          " + vehiculePresent;
        else return "Transporteur";
    } //toString()
} //class PlaceTransporteur
