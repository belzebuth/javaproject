/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import parking.exception.ParkingPleinException;
import parking.exception.PlaceDisponibleException;
import parking.exception.PlaceLibreException;
import parking.exception.PlaceOccupeException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author remyk_000
 */
public class Parking extends Object implements Serializable {
    
    final private static Parking parkingUnique = new Parking(new Double(0),new Double(0),new Double(0),new Double(0),0,0);
    private  Map<Integer,Place> theParking;
    private Map<String, Utilisateur> utilisateurs = new HashMap<String, Utilisateur>();
    private Double tarifParticulier;
    private Double tarifTransporteur;
    private Double tarifReservation;
    private Double tarifHoraire;
    private int nbFacture;
//===========================================================================================================
/*___________________________CONSTRUCTOR___________________________________________________________________*/
//===========================================================================================================
   
    private Parking(Double tPart,Double tTrans,Double tRes,Double tHeure,Integer pPar,Integer pTrans)
    {
        configParking(tPart,tTrans,tRes,tHeure,pPar,pTrans);
    }//Parking()
    
    

//===========================================================================================================
/*___________________________GETTERS AND SETTERS_______________________________________________________________*/
//===========================================================================================================
    
    
    public Double getTarifHoraire() {
        return tarifHoraire;
    }

    public Double getTarifReservation() {
        return tarifReservation;
    }

    public Double getTarifTransporteur() {
        return tarifTransporteur;
    }

    public Double getTarifParticulier()
    {
        return tarifParticulier;
    }

    public static Parking getInstance() {
        if(parkingUnique == null){
            return new Parking(new Double(0),new Double(0),new Double(0),new Double(0),0,0);
        }
            return parkingUnique;
    } //getInstance()
  
    
   public Map<Integer,Place> getTheParking()
   {
       return this.theParking;
   } //getTheParking()

    public Map<String, Utilisateur> getUtilisateurs() {
        return utilisateurs;
    }
   
   public void addUtilisateurs(Utilisateur nouvUtilisateur){


       utilisateurs.put(nouvUtilisateur.getClient(), nouvUtilisateur);
   }
   
//===========================================================================================================
/*___________________________METHODS___________________________________________________________________*/
//===========================================================================================================

    public void configParking(Double tPart,Double tTrans,Double tRes,Double tHeure,Integer pPar,Integer pTrans)
    {
        Double tarifPart = new Double(0);
        
        tarifPart = tPart;
        Double tarifTrans = new Double(0);
        
        tarifTrans = tTrans;
        Double tarifRes = new Double(0);
        
        tarifRes = tRes;
        Double tarifHeure = new Double(0);
        
        tarifHeure = tHeure;
        Integer nbPlacePar = 0;
        
        nbPlacePar = pPar;
        Integer nbPlaceTrans = 0;
        
        nbPlaceTrans = pTrans;

        this.tarifParticulier = tarifPart;
        this.tarifTransporteur = tarifTrans;
        this.tarifReservation = tarifRes;
        this.tarifHoraire = tarifHeure;
        this.theParking = new HashMap();
        this.nbFacture = 0;
        System.out.println(" \n\nCreation du parking");

        for (Integer i = 1; i <= nbPlacePar; ++i)
        {
            theParking.put(i, new PlaceParticulier());
            System.out.println("Particulier ajoute "+i);
        }
        System.out.println("");

        for (Integer i =1; i <= nbPlaceTrans; ++i)
        {
            theParking.put(i + nbPlacePar, new PlaceTransporteur());

            System.out.println("Transporteur ajoute "+i);
        }
        //System.out.println(" \nFin de creation du parking");

    }
    public  boolean vehiculeExiste(String immatriculation)
    {
        boolean existe = false;
        for(Place place : theParking.values())
        {
            if(place.getVehiculePresent().getImmatriculation().equalsIgnoreCase(immatriculation)) existe = true;
        }
        return existe;
    }//vehiculeExiste()
    
    public void park(Vehicule vehicule) throws PlaceOccupeException, ParkingPleinException
    {
        try {

            if (vehicule.getPlaceReservee() != null) {
                vehicule.getPlaceReservee().park(vehicule);
                return;
            } else {

                for (Place place : theParking.values()) {
                    if ((vehicule.getType().equalsIgnoreCase(PlaceParticulier.type) || place.getType().equals(vehicule.getType())) && place.getVehiculePresent() == null && place.getReserve() == null) {
                        place.park(vehicule);
                        return;
                    }
                }

            }
            throw new ParkingPleinException();
        }catch (ParkingPleinException e) {
            System.out.println("Parking plein");
        };
    }

    public void park(Vehicule vehicule, int numeroPlace) throws PlaceOccupeException, ParkingPleinException
    {
        try {
            theParking.get(numeroPlace).park(vehicule);
        }catch (PlaceOccupeException e) {
            System.out.println("Place occup�e");
        };
    }
    
    public Vehicule unpark(Integer numeroPlace) throws PlaceLibreException
    {
        Vehicule vehiculeARetirer = theParking.get(numeroPlace).getVehiculePresent();
        //si place vide
        if(vehiculeARetirer == null)
            throw new PlaceLibreException();
        else
        {
            vehiculeARetirer.setPlaceOccupe(null);
            theParking.get(numeroPlace).setVehiculePresent(null);
            reorganiserPlaces();
            return vehiculeARetirer;
        }
    }//unpark()
    
    public void etatParking()
    {
        System.out.println("Voilà l'état du parking \n");
        for(Integer i = 1 ; i < theParking.size() ; ++i)
        {
            System.out.println("\n Numéro de la place : "+i +" "+ theParking.get(i).toString() + "\n");
        }
        System.out.println("\n Fin du parking ---------------------\n");
    }//etatParking()
    
    public void freePlace(Integer numeroPlace) throws PlaceDisponibleException // a verifier 
    {
        try {
            if (theParking.get(numeroPlace).getReserve() == theParking.get(numeroPlace).getVehiculePresent()) {
                theParking.get(numeroPlace).getVehiculePresent().setPlaceOccupe(null);
                theParking.get(numeroPlace).setVehiculePresent(null);
            } else throw new PlaceDisponibleException();

        }catch (PlaceDisponibleException e) {
            System.out.println ("Place disponible");
        }
    }//freePlace()
    
    public Vehicule retirerVehicule(String immatricluation)
    {
        Vehicule vehiculeARetirer = null;
        
        for(Place place : theParking.values())
        {
            if(place.getVehiculePresent().getImmatriculation().equalsIgnoreCase(immatricluation))
            {
                vehiculeARetirer = place.getVehiculePresent();
                vehiculeARetirer.setPlaceOccupe(null);
                place.setVehiculePresent(null);
                reorganiserPlaces();
                break;
            }
        }
        return vehiculeARetirer;
    }//retirerVehicule()
    
    public void reorganiserPlaces()
    {
        ArrayList<Place> placeLibre = new ArrayList();
        for(Place place : theParking.values())
        {
            
            if(place.getType().equals(PlaceParticulier.type) && place.getVehiculePresent() == null){
                
                placeLibre.add(place);
            }
        }
        
        int nbPlaceLibre = placeLibre.size() - 1;
        
        for(Integer i = 1 ; i <= theParking.size()  && nbPlaceLibre >= 0 ; ++i )
        {
            if(theParking.get(i).getVehiculePresent() != null) 
            {
                if (theParking.get(i).getType().equals(PlaceTransporteur.type) && theParking.get(i).getVehiculePresent().getType().equals(PlaceParticulier.type)) 
                {
                    theParking.get(i).getVehiculePresent().setPlaceOccupe(placeLibre.get(nbPlaceLibre));
                    placeLibre.get(nbPlaceLibre).setVehiculePresent(theParking.get(i).getVehiculePresent());
                    theParking.get(i).setVehiculePresent(null);
                    --nbPlaceLibre;
                }
            }
        }
    }//reorganiserPlaces()

    @Override
    public String toString() {
        return "Parking{" + "theParking=" + theParking + ", utilisateurs=" + utilisateurs + ", tarifParticulier=" + tarifParticulier + ", tarifTransporteur=" + tarifTransporteur + ", tarifReservation=" + tarifReservation + ", tarifHoraire=" + tarifHoraire + ", nbFacture" + nbFacture + '}';
    }
    
     public void sauvegarderParking()
    {
         try
        {
            FileOutputStream fout = new FileOutputStream("ParkingSave.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(this);
            oos.close();
            System.out.println("\n Ecriture faite---------Parking Sauvegardé ! ");
                    
        }
        
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.out.println("\n Echec d'écriture ! ");
        }
    }//sauvergarderParking()


    protected void setTheParking(Map<Integer, Place> theParking) {
        this.theParking = theParking;
    }

    protected void setUtilisateurs(Map<String, Utilisateur> utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    protected void setTarifParticulier(Double tarifParticulier) {
        this.tarifParticulier = tarifParticulier;
    }

    protected void setTarifTransporteur(Double tarifTransporteur) {
        this.tarifTransporteur = tarifTransporteur;
    }

    protected void setTarifReservation(Double tarifReservation) {
        this.tarifReservation = tarifReservation;
    }

    protected void setTarifHoraire(Double tarifHoraire) {
        this.tarifHoraire = tarifHoraire;
    }
    
    
    
      public int getNbFacture(){
    return nbFacture;
}
   public void setNbFacture(int nbFacture){
       this.nbFacture = nbFacture;
   } 


}//class Parking
