/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import java.io.Serializable;
import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;
import parking.exception.PlusAucunePlaceException;

/**
 *
 * @author aze
 */
public abstract class Vehicule  implements Serializable{
    protected String immatriculation;
    protected String marque;
    protected String modele;
    protected Utilisateur proprietaire;
    protected Place placeOccupe;
    protected Place placeReservee;
    protected int ptsFidelite;
    //date & heure d'entree
    protected long tempsFacture = 0;
    
    public Vehicule(String immatriculation, String marque, String modele, String utilisateur, int ptsFidelite) {
        Utilisateur proprio = null;
        if(Parking.getInstance().getUtilisateurs() != null) {
            for (Utilisateur user : Parking.getInstance().getUtilisateurs().values()) {
                if (user.getClient().equalsIgnoreCase(utilisateur)) proprio = user;
            }
        }
        if (proprio == null) proprio = new Utilisateur(utilisateur);
        this.immatriculation = immatriculation;
        this.marque = marque;
        this.modele = modele;
        this.proprietaire = proprio;
        this.ptsFidelite = ptsFidelite;
        proprio.addVehicule(this);
    }
//===========================================================================================================
/*___________________________GETTERS AND SETTERS___________________________________________________________________*/
//===========================================================================================================
    
    public abstract String getType();
    
    /**
     *
     * @return
     */
    public Place getPlaceOccupe() {
        return placeOccupe;
    }

    /**
     *
     * @param placeOccupe
     */
    public void setPlaceOccupe(Place placeOccupe) {
        this.placeOccupe = placeOccupe;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public Place getPlaceReservee() {
        return placeReservee;
    }
    // setplacereservee n'est pas utilise
    public void setPlaceReservee(Place placeReservee) {
        this.placeReservee = placeReservee;
    }

    public int getPtsFidelite() {
        return ptsFidelite;
    }

    public void updatePtsFidelite(){
        setPtsFidelite(ptsFidelite + 1);
    }
    
    public void setPtsFidelite(int ptsFidelite) {
        this.ptsFidelite = ptsFidelite;
    }
    
    public long getTempsFacture() 
    {
        return tempsFacture;
    }//getTempsFacture()
    public void setTempsFacture(long tempsFacture)
    {
        this.tempsFacture = tempsFacture;
    }//setTempsFacture()
    
//===========================================================================================================
/*___________________________METHODS___________________________________________________________________*/
//===========================================================================================================
    public int getLocation (int Immatriculation) 
    {
        for(int i = 1; i < Parking.getInstance().getTheParking().size() ; ++i)
        {
            //On parcourt le parking
            //Si la place courante est la place uccpée par le véhicule, on retourn le numéro de la place.
            if(Parking.getInstance().getTheParking().get(i) == this.placeOccupe) return i;
        }
        //Sinon, on retourn -1.
        return -1;
    }//getLocation()
    
    public abstract Place bookPlace()throws PlusAucunePlaceException;


    public void setProprietaire(Utilisateur proprietaire) {
        this.proprietaire = proprietaire;
    }

    @Override
    public String toString() {
        return "Vehicule{" + "immatriculation=" + immatriculation + ", marque=" + marque + ", modele=" + modele + '}';
    } //toString()
    
} //class Vehicule
