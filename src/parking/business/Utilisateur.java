/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author aze
 */
public class Utilisateur implements Serializable {
    private final String client;
    private Map<String, Vehicule> sesVehicules = new HashMap<String, Vehicule>();

    public Utilisateur(String client) {
        this.client = client;
        this.sesVehicules = new HashMap<String,Vehicule>();
        Parking.getInstance().addUtilisateurs(this);
    }
    
    public void addVehicule(Vehicule newVehicule){
        sesVehicules.put(newVehicule.getImmatriculation(), newVehicule);
    }

    public Map<String, Vehicule> getSesVehicules() {
        return sesVehicules;
    }

    public String getClient() {
        return client;
    }

    @Override
    public String toString() {
        return "Utilisateur{" + "client=" + client + ", sesVehicules=" + sesVehicules + '}';
    }

}
