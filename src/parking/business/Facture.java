/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import java.io.Serializable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author remyk_000
 */
public class Facture implements Serializable 
{
    //Numéro de la facture 
    private int numeroF;
    //Montant de la facture
    private double montant;
    //Véhicule correspondant à la facture
    private Vehicule client;
    //Temps entre la facturation et la prise de place/réservation
    private long tempsAFacturer;   
    //Sert à convertir le temps courant récupéré (millisecondes) en heures
    public static final int convertisseur = 1000*60*60 ;
//===========================================================================================================
/*___________________________CONSTRUCTOR___________________________________________________________________*/
//===========================================================================================================
   
    
    public Facture(Vehicule client, Place place)throws IOException
    {
        this.tempsAFacturer  = System.currentTimeMillis()/convertisseur - client.getTempsFacture()/convertisseur;
        
        this.numeroF = Parking.getInstance().getNbFacture() + 1;
        Parking.getInstance().setNbFacture(numeroF);
        this.client = client;
        this.montant = calculerMontant(client);
        
        client.setPtsFidelite(1 + client.getPtsFidelite() );
        this.genererFacture();
    }  //Facture()
    
//===========================================================================================================
/*___________________________GETTERS AND SETTERS_______________________________________________________________*/
//===========================================================================================================
 public long getTempsAFacturer()
 {
        return tempsAFacturer;
 }//getTempsAFacturer()
    

//===========================================================================================================
/*___________________________METHODS___________________________________________________________________*/
//===========================================================================================================

    public double calculerMontant(Vehicule client)
    {
        double prixBase = Parking.getInstance().getTarifParticulier();
        double reservation = 1;
        double tarifHoraire = Parking.getInstance().getTarifHoraire();
        
        if (client.getType().equalsIgnoreCase(PlaceTransporteur.type))
        {   //Si le véhicule est un transporteur, on lui met le tarif adéquat.
            prixBase = Parking.getInstance().getTarifTransporteur();
        }
        if (null != client.getPlaceReservee())
        {   //Si la place était réservée, on rajoute la valeur du tarif de réservation.
            reservation = Parking.getInstance().getTarifReservation();
        }
        //Prix Facturé final incluant la TVA.
        return (prixBase * reservation + tarifHoraire/**heures*/) * 1.196;
    } //calculerMontant()
    
    private void genererFacture () throws IOException {
    	File dossierFacture = new File ("JavaProject/Factures");
    	dossierFacture.mkdirs();
    	File factureTxt = new File("JavaProject/Factures/"+ this.numeroF+ ".txt");

    	
    	try {
    		FileWriter fw = new FileWriter (factureTxt, true);
    		BufferedWriter bw = new BufferedWriter ( fw ) ; 
    		bw.newLine(); 
    		PrintWriter pw = new PrintWriter ( bw ) ; 
    		pw. print ( "Facture : " + this ) ;
    		pw.flush();
    		pw. close ( ) ; 
    		fw.close();
    	}
    	catch (IOException e) {
    		System.out.println("Probleme...");
    	}
    }
    
    
    @Override
    public String toString()
    {
        return "\n Facture N°"+numeroF+ "\n Client"+ client.getImmatriculation() + "\n Montant : "+calculerMontant(client)+"\n Points de fidélité : "+client.getPtsFidelite()+"\n Au revoir.";
    }//toString()
}//class Facture
