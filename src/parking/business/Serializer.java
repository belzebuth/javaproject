/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import parking.exception.fileNotFoundException;

import java.io.*;

/**
 *
 * @author remyk_000
 */
public class Serializer {
    
    public static void serializeParticulier(Parking parking) throws IOException
    {
        try
        {
            FileOutputStream fout = new FileOutputStream("ParkingSave.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(parking);
            oos.close();
            System.out.println("\n Ecriture faite");
                    
        }
        
        catch(Exception ex)
        {
            ex.printStackTrace();
            System.out.println("\n Echec d'écriture ! ");
        }
    }//serializeParticulier()

    public static void readParticulier(Parking parking){
        try {
            File file = new File("ParkingSave.ser");
            if (file.exists()) {


                FileInputStream fiut = new FileInputStream("ParkingSave.ser");
                ObjectInputStream ois = new ObjectInputStream(fiut);
                parking = (Parking) ois.readObject();
                System.out.println(parking.toString());
                Parking.getInstance().setTheParking(parking.getTheParking());

                Parking.getInstance().setTarifParticulier(parking.getTarifParticulier());
                Parking.getInstance().setTarifTransporteur(parking.getTarifTransporteur());
                Parking.getInstance().setTarifHoraire(parking.getTarifHoraire());
                Parking.getInstance().setTarifReservation(parking.getTarifReservation());
                Parking.getInstance().setNbFacture(parking.getNbFacture());

                for(Utilisateur i : parking.getUtilisateurs().values()){
                    for(Vehicule a :i.getSesVehicules().values()){
                        a.setProprietaire(i);
                    }
                }
                Parking.getInstance().setUtilisateurs(parking.getUtilisateurs());
                ois.close();
            }
            else
                throw new fileNotFoundException();
        } catch (fileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }//readParticulier()
    
}
