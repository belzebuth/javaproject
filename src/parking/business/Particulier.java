/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;
import parking.exception.PlusAucunePlaceException;

/**
 *
 * @author aze
 */
public class Particulier extends Vehicule {
    
    static final String type = "particulier";
    
    public Particulier(String immatriculation, String marque, String modele, String proprietaire, int ptsFidelite) 
    {
        super(immatriculation, marque, modele, proprietaire, ptsFidelite);
    } //Particulier()

    @Override
    public String getType(){
        return Particulier.type;
    } //getType()
    
    @Override
    public Place bookPlace() throws PlusAucunePlaceException
    {
        try {
            for (Place place : Parking.getInstance().getTheParking().values()) {
                //On parcourt toutes les places du parking.
                //Si la place est de type particulier et qu'elle n'est pas réservée ni occupée...
                if (place.getType().equalsIgnoreCase(type) && null == place.getVehiculePresent() && null == place.getReserve()) {
                    //...on la réserve
                    place.setReserve(this);
                    this.placeReservee = place;
                    //on initialise le temps à partir duquel est faite la réservation
                    this.tempsFacture = System.currentTimeMillis();
                    //On retrourne la place.
                    return place;
                }
            }
            //Si aucune place libre n'a été trouvée, on sera sorti de la boucle sans retrourner le parking, on lance une exception.
            throw new PlusAucunePlaceException();

        }catch (PlusAucunePlaceException e) {
            System.out.println("Place libre");
        };
        return null;
    } //bookPlace()

   
    

    
}