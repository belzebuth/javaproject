/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.business;

import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;

/**
 *
 * @author aze
 */
public class PlaceParticulier extends Place{
    static final String type = "particulier";
    

    public PlaceParticulier() {
        super();
    } //PlaceParticulier()
        
    
    @Override
    public String getType(){
        return PlaceParticulier.type;
    } //getType()
    

    @Override
    public void park(Vehicule vehicule) throws PlaceOccupeException, ParkingPleinException {
        //Un transporteur ne peut pas se garer sur une place Particulier.
        try {
            if (vehicule.getType().equals(PlaceTransporteur.type)) throw new PlaceOccupeException();
        }catch (PlaceOccupeException e) {
            System.out.println ("Mauvaise place");
            return;
        };
        //Si la place à déjà un véhicule garé, on lance une exception.
        try {
            if (null != this.vehiculePresent) throw new PlaceOccupeException();
        }catch (PlaceOccupeException e) {
            System.out.println ("Place occup�e");
            return;
        };
        try {
            if (this.reserve != null && this.reserve != vehicule) throw new PlaceOccupeException();
        }catch (PlaceOccupeException e) {
            System.out.println ("Place reserv�e");
            return;
        }

            //Sinon, on gare le véhicule sur la place.
            vehicule.setPlaceOccupe(this);
            this.setVehiculePresent(vehicule);

    }//park()
    
    
    @Override
    public String toString() 
    {
        if (vehiculePresent != null) return "Particulier" +"\n          " + vehiculePresent;
        else return "Particulier";
    } //toString()
} //class PlaceParticulier
