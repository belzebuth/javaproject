/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author remyk_000
 */
public class PlaceLibreException extends Exception{
    
    public PlaceLibreException()
    {
        System.err.println("\n Cette place est déjà libre.");
        new ExceptionPopUp("Place déja libre");
    }//PlaceLibreException()
    
}//class PlaceLibreException