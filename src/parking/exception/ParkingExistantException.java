/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author aze
 */
public class ParkingExistantException extends Exception {

    public ParkingExistantException() {
        System.err.println("\n Le parking existe deja");
        new ExceptionPopUp("Le parking existe déja");
    }
    
}
