/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author remyk_000
 */
public class PlaceOccupeException extends Exception {
    
    public PlaceOccupeException()
    {
        System.err.println("\nVous ne pouvez pas vous garer là.");
        new ExceptionPopUp("Impossible de ce garer ici");
    }//PlaceOccupeException()
    
}//class PlaceOccupeException