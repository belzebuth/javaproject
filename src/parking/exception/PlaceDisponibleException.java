/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author remyk_000
 */
public class PlaceDisponibleException extends Exception {
    
    public PlaceDisponibleException()
    {
        System.err.println("\n Vous n'avez pas réservé ");
        new ExceptionPopUp("Aucune réservation pour ce vehicule");
    }//PlaceDisponibleException()
    
}//class PlaceDisponibleException