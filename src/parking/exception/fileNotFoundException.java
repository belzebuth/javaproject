package parking.exception;

import parking.GUI.CreationParking;

import javax.swing.*;
import java.io.FileNotFoundException;

/**
 * Created by Benj on 15/01/2015.
 */
public class fileNotFoundException extends FileNotFoundException {
    public fileNotFoundException() {
        JDialog addPark = new JDialog();
        addPark.setTitle("Creez un parking");
        addPark.setModal(true);
        addPark.add(new CreationParking());

        addPark.pack();
        addPark.setVisible(true);
    }
}
