/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author remyk_000
 */
public class ParkingPleinException extends Exception{
    
    public ParkingPleinException()
    {
        System.err.println("\n Le parking n'a plus aucune place de libre");
        new ExceptionPopUp("Parking plein");
    }//ParkingPleinException()
    
}//class ParkingPleinException