/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parking.exception;

import parking.GUI.ExceptionPopUp;

/**
 *
 * @author remyk_000
 */
public class PlusAucunePlaceException  extends Exception{
    
    public PlusAucunePlaceException()
    {
        System.err.println("\n Toutes les places sont déjà réservées");
        new ExceptionPopUp("Route les places sont réservées");
    }//PlusAucunePlaceException()
    
}//class PlusAucunePlaceException