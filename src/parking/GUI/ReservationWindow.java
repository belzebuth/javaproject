package parking.GUI;

import parking.business.Parking;
import parking.business.Place;
import parking.business.Utilisateur;
import parking.business.Vehicule;
import parking.exception.PlusAucunePlaceException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 17/01/2015.
 */
public class ReservationWindow extends JDialog {

    ReservationWindow(){
        setLayout(new GridLayout(0,2));
        setModal(true);

        add(new JLabel("Reserver pour:"));
        final JComboBox<Vehicule> comboVehi = new JComboBox<Vehicule>();

        for(Utilisateur i : Parking.getInstance().getUtilisateurs().values()){
            for(Vehicule a: i.getSesVehicules().values()){
                comboVehi.addItem(a);

            }
        }

        add(comboVehi);
        JButton reservation = new JButton("Reservation");
        reservation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vehicule vehi = (Vehicule) comboVehi.getSelectedItem();
                try {
                    vehi.bookPlace();
                } catch (PlusAucunePlaceException e1) {
                    e1.printStackTrace();
                }
                dispose();
            }
        });

        add(reservation);

        setSize(300,100);

        setVisible(true);
    }

}
