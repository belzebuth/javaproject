package parking.GUI;

import parking.business.Place;
import parking.business.Vehicule;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 14/01/2015.
 */
public class PlaceButton extends JButton {
    private Place place;
    private Integer numero = new Integer(0);

    public PlaceButton(int num, Place pl){
        place = pl;
        numero = num;
        setSize(10, 10);

        if ("particulier" == place.getType()){
            setText(numero + ": P");
        }
        else{
            setText(numero + ": T");
        }
        update();

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new PlaceDetailWindow(numero,place);
            }
        });

    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public void update(){
        if(null != place.getVehiculePresent()){
            setBackground(new Color(255,0,0));
        }
        else if (null != place.getReserve()){
            setBackground(new Color(0,0,255));
        }
        else{
            setBackground(new Color(255,255,255));
        }

        invalidate();
        revalidate();
        repaint();
    }


}
