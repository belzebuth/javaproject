package parking.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 18/01/2015.
 */
public class ExceptionPopUp extends JDialog {
    public ExceptionPopUp(String str){
        setTitle("Exception");
        setLayout(new GridLayout(0,1));
        add(new JTextArea(str));
        setModal(true);

        JButton ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        pack();
        setVisible(true);
    }
}
