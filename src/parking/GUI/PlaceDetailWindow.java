package parking.GUI;

import parking.business.Facture;
import parking.business.Parking;
import parking.business.Place;
import parking.exception.PlaceLibreException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Benj on 14/01/2015.
 */
public class PlaceDetailWindow extends JDialog {
    PlaceDetailWindow(final Integer numb, final Place place){
        super();
        setLayout(new GridLayout(0,2));
        setModal(true);

        JLabel num = new JLabel("Numero Place :");
        add(num);
        JLabel nume = new JLabel(numb.toString());
        add(nume);

        JLabel res = new JLabel("Reservé par :");
        add(res);
        JLabel reserv = new JLabel();
        if(place.getReserve() != null){
            reserv.setText(place.getReserve().getImmatriculation());
        }
        add(reserv);

        JLabel occup = new JLabel("Occupé par :");
        add(occup);
        JLabel occupe = new JLabel();
        if(place.getVehiculePresent() != null){
            occupe.setText(place.getVehiculePresent().getImmatriculation());
        }
        add(occupe);

        if(place.getReserve()!=null) {

            JButton unReserv = new JButton("Enlever reservation");
            unReserv.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    place.setReserve(null);
                    dispose();
                }
            });
            add(unReserv);
        }
        if(place.getVehiculePresent() != null){
            final JButton facturer = new JButton("Facturer et quitter place");
            facturer.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        Facture fact = new Facture(place.getVehiculePresent(),place);


                        Parking.getInstance().unpark(numb);
                        new FactureWindow(fact);
                        dispose();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (PlaceLibreException e1) {
                        e1.printStackTrace();
                    }

                }
            });
            add(facturer);
        }
        pack();
        setVisible(true);

    }
}