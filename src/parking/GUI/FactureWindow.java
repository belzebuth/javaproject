package parking.GUI;

import parking.business.Facture;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 13/01/2015.
 */
public class FactureWindow extends JDialog{
    
    FactureWindow(final Facture fact){

        setTitle("Facture");
        setModal(true);

        GridLayout grid = new GridLayout(0,1);
        grid.setHgap(5);
        grid.setVgap(5);
        setLayout(grid);

        JTextArea immat = new JTextArea(fact.toString());
        add(immat);



        JButton generer = new JButton("Imprimer la facture");
        generer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        add(generer);
        
        
        pack();
        setVisible(true);
    }
}
