package parking.GUI;

import parking.business.Parking;
import parking.business.Particulier;
import parking.business.Transporteur;
import parking.business.Vehicule;
import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 13/01/2015.
 */
public class AddVehicleWindow {

    private JTextField imm = new JTextField();
    private JTextField marque = new JTextField();
    private JTextField model = new JTextField();
    private JTextField proprio = new JTextField();
    JDialog addWin = new JDialog();

    AddVehicleWindow(){

        addWin.setTitle("Ajouter un vehicule");
        GridLayout grid = new GridLayout(6,2);
        grid.setHgap(5);
        grid.setVgap(5);
        addWin.setLayout(grid);

        addWin.setModal(true);

        JLabel type = new JLabel();
        type.setText("Type de vehicule :");

        addWin.add(type);

        final JComboBox typeChoose = new JComboBox();
        typeChoose.addItem("Particulier");
        typeChoose.addItem("Transporteur");
        addWin.add(typeChoose);

        JLabel imma = new JLabel();
        imma.setText("Immatriculation :");
        addWin.add(imma);


        addWin.add(imm);

        final JLabel marq = new JLabel();
        marq.setText("Marque :");
        addWin.add(marq);


        addWin.add(marque);

        JLabel mod = new JLabel();
        mod.setText("Modèle :");
        addWin.add(mod);


        addWin.add(model);

        JLabel prop = new JLabel();
        prop.setText("Propriétaire :");
        addWin.add(prop);


        addWin.add(proprio);

        JButton add = new JButton("Ajouter");
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Vehicule vehi;
                System.out.println("valeur "+imm.getText());

                if(!imm.getText().isEmpty() && !marque.getText().isEmpty() && !model.getText().isEmpty() && !proprio.getText().isEmpty()) {
                    if ("Particulier" == typeChoose.getSelectedItem()) {
                        vehi = new Particulier(imm.getText(), marque.getText(), model.getText(), proprio.getText(), 0);
                    } else {
                        vehi = new Transporteur(imm.getText(), marque.getText(), model.getText(), proprio.getText(), 0);
                    }
                    try {
                        Parking.getInstance().park(vehi);
                    } catch (PlaceOccupeException e1) {
                        e1.printStackTrace();
                    } catch (ParkingPleinException e1) {
                        e1.printStackTrace();
                    }

                    addWin.dispose();
                }
                else{
                    JOptionPane jop1 = new JOptionPane();

                    jop1.showMessageDialog(null, "Information manquantes", "ATTENTION", JOptionPane.WARNING_MESSAGE);

                }


            }
        });
        addWin.add(add);



        addWin.pack();
        addWin.setVisible(true);
    }
}
