package parking.GUI;

import parking.business.Parking;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 17/01/2015.
 */
public class menuBar extends JMenuBar {

    menuBar(){

        JMenu fichier = new JMenu();
        fichier.setText("Fichier");
        JMenu action = new JMenu("Action");

        JMenuItem addV = new JMenuItem();
        addV.setText("Ajouter un véhicule");
        addV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AddVehicleWindow();
            }
        });
        fichier.add(addV);


        JMenuItem arrVehi = new JMenuItem();
        arrVehi.setText("Arrivée vehicule");
        arrVehi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new arriveeWindow();
            }
        });

        fichier.add(arrVehi);

        JMenuItem reOrg = new JMenuItem("Reorganiser parking");
        reOrg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Parking.getInstance().reorganiserPlaces();
                JDialog update = new JDialog();
                update.setVisible(true);
                update.dispose();
            }
        });
        action.add(reOrg);

        JMenuItem reserv = new JMenuItem("Reserver une place");
        reserv.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ReservationWindow();
            }
        });
        action.add(reserv);

        add(fichier);
        add(action);
    }
}
