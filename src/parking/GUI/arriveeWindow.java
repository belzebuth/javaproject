package parking.GUI;

import parking.business.Parking;
import parking.business.Utilisateur;
import parking.business.Vehicule;
import parking.exception.ParkingPleinException;
import parking.exception.PlaceOccupeException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Benj on 17/01/2015.
 */
public class arriveeWindow extends JDialog {

    public arriveeWindow(){
        setLayout(new GridLayout(0,2));
        setModal(true);

        add(new JLabel("Vehicule arrivé :"));

        final JComboBox<Vehicule> comboVehi = new JComboBox<Vehicule>();


        for(Utilisateur i : Parking.getInstance().getUtilisateurs().values()){
            for(Vehicule a: i.getSesVehicules().values()){
                comboVehi.addItem(a);

            }
        }
        add(comboVehi);

        JButton park = new JButton("Garer");
        park.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Parking.getInstance().park((Vehicule) comboVehi.getSelectedItem());
                    dispose();
                } catch (PlaceOccupeException e1) {
                    e1.printStackTrace();
                } catch (ParkingPleinException e1) {
                    e1.printStackTrace();
                }
            }
        });
        add(park);
        pack();
        setVisible(true);
    }
}
