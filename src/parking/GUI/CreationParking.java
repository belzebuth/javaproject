package parking.GUI;

import parking.business.Parking;
import parking.business.Place;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;

/**
 * Created by Benj on 12/01/2015.
 */
public class CreationParking extends JPanel {

    

   
    private JTextField tParticulier = new JTextField();
    private JTextField tTransporteurs = new JTextField();
    private JTextField tReservation = new JTextField();
    private JTextField tHeure = new JTextField();
    private JTextField nbPar = new JTextField();
    private JTextField nbTrans = new JTextField();



    public CreationParking() {



        GridLayout grid = new GridLayout(7,2);

        grid.setHgap(5);
        grid.setVgap(5);

        setLayout(grid);

        JLabel tPart = new JLabel();
        tPart.setText("Tarif Particulier :");


        add(tPart);



        add(tParticulier);



        JLabel tTrans = new JLabel();
        tTrans.setText("Tarif Transporteurs :");
        add(tTrans);




        add(tTransporteurs);

        JLabel tRes = new JLabel();
        tRes.setText("Tarif Reservations :");
        add(tRes);


        add(tReservation);

        JLabel tH = new JLabel();
        tH.setText("Tarif horaire :");
        add(tH);


        add(tHeure);

        JLabel nbP = new JLabel();
        nbP.setText("Places particuliers :");
        add(nbP);


        add(nbPar);

        JLabel nbT = new JLabel();
        nbT.setText("Place transporteurs :");
        add(nbT);


        add(nbTrans);

        JButton send = new JButton("Creer");


        send.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Parking.getInstance().configParking(Double.parseDouble(tParticulier.getText()), Double.parseDouble(tTransporteurs.getText()), Double.parseDouble(tReservation.getText()), Double.parseDouble(tHeure.getText()), Integer.parseInt(nbPar.getText()), Integer.parseInt(nbTrans.getText()));
                ((Window) getRootPane().getParent()).dispose();

            }
        });
        add(send);


    }


}
