package parking.GUI;

import parking.business.Parking;
import parking.business.Serializer;

import javax.swing.event.AncestorEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Created by Benj on 15/01/2015.
 */
public abstract class winListener implements WindowListener {
    @Override
    public void windowOpened(WindowEvent e) {

        //Serializer.readParticulier(Parking.getInstance());
    }

    @Override
    public void windowClosing(WindowEvent e) {
        Parking.getInstance().sauvegarderParking();

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }


    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
