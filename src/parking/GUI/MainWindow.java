package parking.GUI;

import parking.business.Parking;
import parking.business.Serializer;

import javax.swing.*;
import java.awt.event.WindowEvent;

/**
 * Created by Benj on 11/01/2015.
 */
public class MainWindow {
    MainWindow(){
        JFrame win = new JFrame();
        win.setTitle("Données parking");
        win.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        menuBar menu = new menuBar();

        win.setJMenuBar(menu);

        final ParkDetailWindow dwin =new ParkDetailWindow();

        win.addWindowListener(new winListener() {
            @Override
            public void windowActivated(WindowEvent e) {
                dwin.update();
            }
        });



        win.add(dwin);
        win.pack();
        win.setVisible(true);
    }



    public static void main(String[] args) {

        Serializer.readParticulier(Parking.getInstance());
        MainWindow main = new MainWindow();
    }
}