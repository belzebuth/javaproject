package parking.GUI;

import parking.business.Parking;
import parking.business.Place;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

/**
 * Created by Benj on 14/01/2015.
 */
public class ParkDetailWindow extends JPanel {
    
    private JLabel tarifPart = new JLabel();
    private JLabel tarifTrans = new JLabel();
    private JLabel tarifRes = new JLabel();
    private JLabel tarifHor = new JLabel();
    private JLabel nbPlacePart = new JLabel();
    private JLabel nbPlaceTrans = new JLabel();
    private JLabel placePartDispo = new JLabel();
    private JLabel placeTransDispo = new JLabel();
    private JLabel nbPlace = new JLabel();
    private JLabel placeDispo = new JLabel();
    private ArrayList<PlaceButton> liste = new ArrayList<PlaceButton>();
    ParkDetailWindow(){
        int number = 1;
        for (Place i : Parking.getInstance().getTheParking().values()){
            PlaceButton PButton = new PlaceButton(number,i);

            liste.add(PButton);
            ++number;
        }

        update();
        setLayout(new GridLayout(2, 1));


        JPanel tab = new JPanel();
        tab.setLayout(new GridLayout(2,0));


        JLabel tP = new JLabel("Tarif Particuliers :");
        tab.add(tP);
        tab.add(tarifPart);

        JLabel tR = new JLabel("Tarif Reservation :");
        tab.add(tR);
        tab.add(tarifRes);

        JLabel pTot = new JLabel("Nombre de Places :");
        tab.add(pTot);
        tab.add(nbPlace);

        JLabel pP = new JLabel("Places Particuliers :");
        tab.add(pP);
        tab.add(nbPlacePart);

        JLabel pT = new JLabel("Places Transporteurs :");
        tab.add(pT);
        tab.add(nbPlaceTrans);

        JLabel tT = new JLabel("Tarif Transporteurs :");
        tab.add(tT);
        tab.add(tarifTrans);

        JLabel tH = new JLabel("Tarif Horaire :");
        tab.add(tH);
        tab.add(tarifHor);

        JLabel pD = new JLabel("Disponibles :");
        tab.add(pD);
        tab.add(placeDispo);

        JLabel pPD = new JLabel("Disponibles :");
        tab.add(pPD);
        tab.add(placePartDispo);

        JLabel pTD = new JLabel("Disponibles :");
        tab.add(pTD);
        tab.add(placeTransDispo);

        JPanel aff = new JPanel(new GridLayout(0,20));
        aff.setMaximumSize(new Dimension(0,800));
        aff.setAutoscrolls(true);

        for(PlaceButton i : liste){
            aff.add(i);
        }


        add(tab);
        add(aff);
        setMaximumSize(new Dimension(1000, 1000));

        setVisible(true);

    }

    public void update(){
        tarifPart.setText(Parking.getInstance().getTarifParticulier().toString());
        tarifTrans.setText(Parking.getInstance().getTarifTransporteur().toString());
        tarifRes.setText(Parking.getInstance().getTarifReservation().toString());
        tarifHor.setText(Parking.getInstance().getTarifHoraire().toString());
        System.out.println("updating");
        Double placePart = new Double(0);
        Double placeTrans = new Double(0);
        Double partDispo = new Double(0);
        Double transDispo = new Double(0);

        for (Place i : Parking.getInstance().getTheParking().values()){
            if("particulier" == i.getType()){
                ++placePart;
                if( null == i.getVehiculePresent()){
                    ++partDispo;
                }
            }
            else{
                ++placeTrans;
                if(null == i.getVehiculePresent()){
                    ++transDispo;
                }
            }
        }
        nbPlacePart.setText(placePart.toString());
        nbPlaceTrans.setText(placeTrans.toString());
        placePartDispo.setText(partDispo.toString());
        placeTransDispo.setText(transDispo.toString());
        nbPlace.setText(new Double(placePart + placeTrans).toString());
        placeDispo.setText(new Double(partDispo + transDispo).toString());
        for(PlaceButton i : liste){
            i.update();
        }

        if(Parking.getInstance().getTheParking().size() > liste.size()){
            for(int i = liste.size(); i <Parking.getInstance().getTheParking().size();++i){
                liste.add(new PlaceButton(i,Parking.getInstance().getTheParking().get(i)));
            }
        }
    }
}
